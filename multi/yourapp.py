import os
import time
from flask import Flask, Response
from prometheus_client import multiprocess
from prometheus_client import generate_latest, CollectorRegistry, CONTENT_TYPE_LATEST, Gauge

app = Flask(__name__)

g1= Gauge("arun","asas")


def update():
    time.sleep(10)
    g1.inc()
@app.route("/metrics")
def metrics():
    update()
    registry = CollectorRegistry()
    multiprocess.MultiProcessCollector(registry)
    data = generate_latest(registry)
    return Response(data, mimetype=CONTENT_TYPE_LATEST)
